package ad.tranxuan.vinh.chatapp.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import ad.tranxuan.vinh.chatapp.Activities.Login.LoginActivity;
import ad.tranxuan.vinh.chatapp.R;

public class AddPostActivity extends AppCompatActivity {
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    ActionBar actionBar;
    //View
    EditText txtPostTitle,txtPostDes;
    ImageView imgPostImage;
    Button btnUpLoad;
    private static final int CAMERA_REQUEST_CODE=100;
    private static final int STORAGE_REQUEST_CODE=200;
    private static final int IMAGE_PICK_GALLERY_CODE=300;
    private static final int IMAGE_PICK_CAMERA_CODE=400;
    String cameraPermisstion[];
    String storagePermisstion[];
    Uri image_uri=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);
        firebaseAuth=FirebaseAuth.getInstance();

        actionBar=getSupportActionBar();
        actionBar.setTitle("Add New Post");
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        cameraPermisstion=new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE};
        storagePermisstion=new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
        //init control view
        addControls();
        addEvents();

    }

    private void addEvents() {
        imgPostImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImagePickDialog();
            }
        });
        btnUpLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void showImagePickDialog() {
        //option to show dialog
        String options[]={"Camera","Gallery"};
        //alert dialog
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        //set title
        builder.setTitle("Pick Image From");
        //set items ti dialog
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //handle dialog item click
                if(i==0){
                    //camera clicked
                    if(!checkCameraPermisstion()){
                        requestCameraPermisstion();
                    }
                    else{
                        pickFromCamera();
                    }
                    showImagePicDialog();
                }else if(i==1){
                    //gallery clicked
                    if(!checkStoragePermisstion()){
                        requestStoragePermisstion();
                    }
                    else {
                        pickFromGallery();
                    }

                }

            }
        });
        builder.create().show();

    }

    private boolean checkStoragePermisstion(){
        //check if storage permisstion is enable or not
        //return true if enable
        //return false if not enabled
        boolean result= ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED;
        return result;
    }

    private void requestStoragePermisstion(){
        requestPermissions(storagePermisstion,STORAGE_REQUEST_CODE);
    }

    private boolean checkCameraPermisstion(){
        //check if storage permisstion is enable or not
        //return true if enable
        //return false if not enabled
        boolean result= ContextCompat.checkSelfPermission(this,Manifest.permission.CAMERA)==(PackageManager.PERMISSION_GRANTED) ;
        boolean result1= ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE)== (PackageManager.PERMISSION_GRANTED);
        return result && result1;
    }

    private void requestCameraPermisstion(){
        requestPermissions(cameraPermisstion,STORAGE_REQUEST_CODE);
    }

    private void pickFromGallery() {
        //Pick from gallery
        Intent galleryIntent=new Intent(Intent.ACTION_PICK);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent,IMAGE_PICK_GALLERY_CODE);

    }

    private void pickFromCamera() {
        //Intent of picking image from device camera
        ContentValues values=new ContentValues();
        values.put(MediaStore.Images.Media.TITLE,"Temp Pic");
        values.put(MediaStore.Images.Media.DESCRIPTION,"Temp Description");
        //put image uri
        image_uri=this.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,values);
        //intent to start camera
        Intent cameraIntent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,image_uri);
        startActivityForResult(cameraIntent,IMAGE_PICK_CAMERA_CODE);

    }

    private void showImagePicDialog() {
        //option to show dialog
        String options[]={"Camera","Gallery"};
        //alert dialog
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        //set title
        builder.setTitle("Pick Image From");
        //set items ti dialog
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //handle dialog item click
                if(i==0){
                    //camera clicked
                    if(!checkCameraPermisstion()){
                        requestCameraPermisstion();
                    }
                    else{
                        pickFromCamera();
                    }
                    showImagePicDialog();
                }else if(i==1){
                    //gallery clicked
                    if(!checkStoragePermisstion()){
                        requestStoragePermisstion();
                    }
                    else {
                        pickFromGallery();
                    }

                }

            }
        });
        builder.create().show();
    }

    private void addControls() {
        txtPostTitle=findViewById(R.id.txtPostTitle);
        txtPostDes=findViewById(R.id.txtPostDes);
        btnUpLoad=findViewById(R.id.btnPostUpLoad);
        imgPostImage=findViewById(R.id.imgPostImage);
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkUserStatus();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkUserStatus();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        menu.findItem(R.id.mnu_add_post).setVisible(true);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==R.id.mnu_logout){
            sendToLogin();
            checkUserStatus();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        /*
        This method called when user press allow or deny from permisstion request dialog
        here we will handle permisstion cases (allow & deny).
         */
        switch (requestCode){
            case CAMERA_REQUEST_CODE:{
                if(grantResults.length>0){
                    //picking from camera first check if camera and storage permisstion allowed or not
                    boolean cameraAccepted=grantResults[0]==PackageManager.PERMISSION_GRANTED;
                    boolean writestorageAccepted=grantResults[1]==PackageManager.PERMISSION_GRANTED;
                    if(cameraAccepted && writestorageAccepted){
                        pickFromCamera();
                    }
                    else {
                        Toast.makeText(this, "Please enable camera and storage permisstion", Toast.LENGTH_SHORT).show();
                    }
                }

            }
            break;
            case STORAGE_REQUEST_CODE:{
                //picking from gallery first check if  storage permisstion allowed or not
                if(grantResults.length>0){
                    boolean writestorageAccepted=grantResults[0]==PackageManager.PERMISSION_GRANTED;
                    if( writestorageAccepted){
                        pickFromGallery();
                    }
                    else {
                        Toast.makeText(this, "Please enable storage permisstion", Toast.LENGTH_SHORT).show();
                    }
                }

            }
            break;

        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode==RESULT_OK ){
            if(requestCode==IMAGE_PICK_GALLERY_CODE){
                image_uri=data.getData();
                imgPostImage.setImageURI(image_uri);

            }
            if(requestCode==IMAGE_PICK_CAMERA_CODE){
                imgPostImage.setImageURI(image_uri);

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void checkUserStatus() {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if(user!=null){

        }
        else{
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

    }

    private void sendToLogin() { //funtion
        GoogleSignInClient mGoogleSignInClient ;
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mGoogleSignInClient.signOut().addOnCompleteListener(this,
                new OnCompleteListener<Void>() {  //signout Google
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        FirebaseAuth.getInstance().signOut(); //signout firebase
                        Intent setupIntent = new Intent(AddPostActivity.this, LoginActivity.class/*To ur activity calss*/);
                        Toast.makeText(AddPostActivity.this, "Logged Out", Toast.LENGTH_LONG).show(); //if u want to show some text
                        setupIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(setupIntent);
                       finish();
                    }
                });
    }
}
