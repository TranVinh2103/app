package ad.tranxuan.vinh.chatapp.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.MenuBuilder;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.internal.GoogleApiManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import ad.tranxuan.vinh.chatapp.Activities.Login.LoginActivity;
import ad.tranxuan.vinh.chatapp.Fragment.ChatListFragment;
import ad.tranxuan.vinh.chatapp.Fragment.HomeFragment;
import ad.tranxuan.vinh.chatapp.Fragment.ProfileFragment;
import ad.tranxuan.vinh.chatapp.Fragment.UsersFragment;
import ad.tranxuan.vinh.chatapp.Notification.Token;
import ad.tranxuan.vinh.chatapp.R;

public class DashboardActivity extends AppCompatActivity {
    private GoogleSignInClient mGoogleSignInClient;
    GoogleApiManager mGoogleApiClient;
    private FirebaseAuth mAuth;
    BottomNavigationView navigationView;
    ActionBar actionBar;
    String mUID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        mAuth= FirebaseAuth.getInstance();
        actionBar=getSupportActionBar();
        addControls();
        addEvents();
        actionBar.setTitle("Home");
        HomeFragment fragment=new HomeFragment();
        FragmentTransaction ft=getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container,fragment,"");
        ft.commit();
        checkUserStatus();

        //update token
        updateToken(FirebaseInstanceId.getInstance().getToken());
    }

    public void updateToken(String token){
        DatabaseReference ref= FirebaseDatabase.getInstance().getReference("Tokens");
        Token mToken=new Token(token);
        ref.child(mUID).setValue(mToken);
    }

    @Override
    protected void onResume() {
        checkUserStatus();
        super.onResume();
    }

    private void addEvents() {
        navigationView.setOnNavigationItemSelectedListener(selectedListener);


    }

    private void addControls() {
        navigationView=findViewById(R.id.navigation);

    }

    private BottomNavigationView.OnNavigationItemSelectedListener selectedListener=
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    switch (menuItem.getItemId()){
                        case R.id.nav_home:
                            actionBar.setTitle("Home");
                            HomeFragment fragment1=new HomeFragment();
                            FragmentTransaction ft1=getSupportFragmentManager().beginTransaction();
                            ft1.replace(R.id.container,fragment1,"");
                            ft1.commit();
                            return true;
                        case R.id.nav_profile:
                            actionBar.setTitle("Profile");
                            ProfileFragment fragment2=new ProfileFragment();
                            FragmentTransaction ft2=getSupportFragmentManager().beginTransaction();
                            ft2.replace(R.id.container,fragment2,"");
                            ft2.commit();
                            return true;
                        case R.id.nav_users:
                            actionBar.setTitle("Users");
                            UsersFragment fragment3=new UsersFragment();
                            FragmentTransaction ft3=getSupportFragmentManager().beginTransaction();
                            ft3.replace(R.id.container,fragment3,"");
                            ft3.commit();
                            return true;
                        case R.id.nav_chatlist:
                            actionBar.setTitle("Chats");
                            ChatListFragment fragment4=new ChatListFragment();
                            FragmentTransaction ft4=getSupportFragmentManager().beginTransaction();
                            ft4.replace(R.id.container,fragment4,"");
                            ft4.commit();
                            return true;
                    }
                    return false;
                }
            };


    @Override
    protected void onStart() {
        super.onStart();
    }
    private void checkUserStatus() {
        FirebaseUser user = mAuth.getCurrentUser();
        if(user!=null){
            mUID=user.getUid();
            //save uid of currently signed in user in shared prefrence
            SharedPreferences sp=getSharedPreferences("SP_USER",MODE_PRIVATE);
            SharedPreferences.Editor editor=sp.edit();
            editor.putString("Current_USERID",mUID);
            editor.apply();

        }
        else{
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
