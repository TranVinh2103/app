package ad.tranxuan.vinh.chatapp.View.Register;

public interface RegisterView {
    void registerValidations();
    void registerSuccess();
    void validateEmail();
    void validatePassWord();
    void validateConfirmPass();


}
