package ad.tranxuan.vinh.chatapp.Fragment;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.view.menu.MenuBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import ad.tranxuan.vinh.chatapp.Activities.AddPostActivity;
import ad.tranxuan.vinh.chatapp.Activities.Login.LoginActivity;
import ad.tranxuan.vinh.chatapp.R;
import static android.app.Activity.RESULT_OK;
import static com.google.firebase.storage.FirebaseStorage.getInstance;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    //firebase
    FirebaseAuth firebaseAuth;
    FirebaseUser user;
    FirebaseDatabase database;
    DatabaseReference databaseReference;
    //storage
    StorageReference storageReference;
    //path where image of user profile and cover will be stored
    String storagePath="Users_Profile_Cover_Img/";
    //view from ProfileFragment.xml
    ImageView imgAvartar,imgCover;
    TextView txtPrfName,txtPrfEmail,txtPrfPhone;
    FloatingActionButton btnFloatAction;
    //ProgressDialog
    ProgressDialog progressDialog;
    //permisstion contamts
    private static final int CAMERA_REQUEST_CODE=100;
    private static final int STORAGE_REQUEST_CODE=200;
    private static final int IMAGE_PICK_GALLERY_CODE=300;
    private static final int IMAGE_PICK_CAMERA_CODE=400;
    //arrays of permisstion to be requested
    String cameraPermisstion[];
    String storagePermisstion[];
    //uri of picked image
    Uri image_uri;
    //for checking profile or cover photo
    String profileOrCoverPhoto;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_profile, container, false);
        //init firebase
        firebaseAuth=FirebaseAuth.getInstance();
        user=firebaseAuth.getCurrentUser();
        database= FirebaseDatabase.getInstance();
        databaseReference=database.getReference("Users");
        storageReference=getInstance().getReference();
        //init View
        addControls(view);
        addEvent();

        /* we have to get info of currently signed in user. we can get it using user's email or uid
        I'm gonna retrieve user detail using email
        By using oderByChild query we will show the detail from a node whose key named has value
        equal to currently signed in email . It will search all nodes,
        where the key matches it will get it's detail
         */
        Query query=databaseReference.orderByChild("email").equalTo(user.getEmail());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds: dataSnapshot.getChildren()){
                    //get data
                    String name=""+ds.child("name").getValue();
                    String email=""+ds.child("email").getValue();
                    String phone=""+ds.child("phone").getValue();
                    String image=""+ds.child("image").getValue();
                    String cover=""+ds.child("cover").getValue();
                    //set data
                    txtPrfName.setText(name);
                    txtPrfEmail.setText(email);
                    txtPrfPhone.setText(phone);

                    try{
                        Picasso.get().load(image).into(imgAvartar);
                    }
                    catch (Exception ex){
                        Picasso.get().load(R.drawable.ic_default_img_white).into(imgAvartar);
                    }
                    try{
                        Picasso.get().load(cover).into(imgCover);
                    }
                    catch (Exception ex){

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return view;
    }
    private void addControls(View view) {
        //init View
        imgAvartar=view.findViewById(R.id.imgAvartar);
        imgCover=view.findViewById(R.id.imgCover);
        txtPrfName=view.findViewById(R.id.txtPrfName);
        txtPrfEmail=view.findViewById(R.id.txtPrfEmail);
        txtPrfPhone=view.findViewById(R.id.txtPrfPhone);
        btnFloatAction=view.findViewById(R.id.btnFloatAction);
        //init ProgressDialog
        progressDialog=new ProgressDialog(getActivity());
        //init arrays of permisstions
        cameraPermisstion=new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE};
        storagePermisstion=new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
    }
    private boolean checkStoragePermisstion(){
        //check if storage permisstion is enable or not
        //return true if enable
        //return false if not enabled
        boolean result= ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED;
        return result;
    }
    private void requestStoragePermisstion(){
       requestPermissions(storagePermisstion,STORAGE_REQUEST_CODE);
    }

    private boolean checkCameraPermisstion(){
        //check if storage permisstion is enable or not
        //return true if enable
        //return false if not enabled
        boolean result= ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.CAMERA)==(PackageManager.PERMISSION_GRANTED) ;
        boolean result1= ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.WRITE_EXTERNAL_STORAGE)== (PackageManager.PERMISSION_GRANTED);
        return result && result1;
    }

    private void requestCameraPermisstion(){
       requestPermissions(cameraPermisstion,STORAGE_REQUEST_CODE);
    }

    private void addEvent() {
       btnFloatAction.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               showEditProfileDialog();
           }
       });

    }

    private void showEditProfileDialog() {
        /*
        Show dialog containing options:
        1)Edit Profile Picture
        2)Edit cover Photo
        3)Edit name
        4)Edit phone
         */
        //option to show dialog
        String options[]={"Edit Profile Picture","Edit Cover Photo","Edit Name","Edit Phone"};
        //alert dialog
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        //set title
        builder.setTitle("Choose Action");
        //set items ti dialog
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //handle dialog item click
                if(i==0){
                    //Edit Profile clicked
                    progressDialog.setMessage("Updating Profile Picture");
                    profileOrCoverPhoto="image";
                    showImagePicDialog();
                }else if(i==1){
                    //Edit Cover
                    progressDialog.setMessage("Updating Cover Photo");
                    profileOrCoverPhoto="cover";
                    showImagePicDialog();
                }
                else if(i==2){
                    //Edit Name
                    progressDialog.setMessage("Updating Name");
                    showNamePhoneUpdateDialog("name");

                }
                else if(i==3){
                    //Edit Phone
                    progressDialog.setMessage("Updating Phone Number");
                    showNamePhoneUpdateDialog("phone");

                }
            }
        });
        builder.create().show();
    }

    private void showNamePhoneUpdateDialog(final String key) {
        /*
        parameter "key" will contain value:
        either "name" wich is key in user's database wich is used to update user's name
        or "phone" which is key in user's database wich is used to update phone
         */
        //custom dialog
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setTitle("Update "+key);
        //set layout of dialog
        LinearLayout linearLayout=new LinearLayout(getActivity());
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setPadding(10,10,10,10);
        // add edit text
        final EditText editText=new EditText(getActivity());
        editText.setHint("Enter "+key);
        linearLayout.addView(editText);
        builder.setView(linearLayout);
        // add button in dialog to update and cancle
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //input text from edittext
                String value=editText.getText().toString().trim();
                //validate if user has entered something or not
                if(!TextUtils.isEmpty(value)){
                    progressDialog.show();
                    HashMap<String,Object> result=new HashMap<>();
                    result.put(key,value);
                    databaseReference.child(user.getUid()).updateChildren(result)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getActivity(), "Updated...", Toast.LENGTH_SHORT).show();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });

                }
                else {
                    Toast.makeText(getActivity(), "Please Enter "+key, Toast.LENGTH_SHORT).show();
                }

            }
        });
        builder.setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.dismiss();

            }
        });
        builder.create().show();

    }


    private void showImagePicDialog() {
        //option to show dialog
        String options[]={"Camera","Gallery"};
        //alert dialog
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        //set title
        builder.setTitle("Pick Image From");
        //set items ti dialog
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //handle dialog item click
                if(i==0){
                    //camera clicked
                    if(!checkCameraPermisstion()){
                        requestCameraPermisstion();
                    }
                    else{
                        pickFromCamera();
                    }
                    showImagePicDialog();
                }else if(i==1){
                    //gallery clicked
                    if(!checkStoragePermisstion()){
                        requestStoragePermisstion();
                    }
                    else {
                        pickFromGallery();
                    }

                }

            }
        });
        builder.create().show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        /*
        This method called when user press allow or deny from permisstion request dialog
        here we will handle permisstion cases (allow & deny).
         */
        switch (requestCode){
            case CAMERA_REQUEST_CODE:{
                if(grantResults.length>0){
                    //picking from camera first check if camera and storage permisstion allowed or not
                    boolean cameraAccepted=grantResults[0]==PackageManager.PERMISSION_GRANTED;
                    boolean writestorageAccepted=grantResults[1]==PackageManager.PERMISSION_GRANTED;
                    if(cameraAccepted && writestorageAccepted){
                        pickFromCamera();
                    }
                    else {
                        Toast.makeText(getActivity(), "Please enable camera and storage permisstion", Toast.LENGTH_SHORT).show();
                    }
                }

            }
            break;
            case STORAGE_REQUEST_CODE:{
                //picking from gallery first check if  storage permisstion allowed or not
                if(grantResults.length>0){
                    boolean writestorageAccepted=grantResults[0]==PackageManager.PERMISSION_GRANTED;
                    if( writestorageAccepted){
                        pickFromGallery();
                    }
                    else {
                        Toast.makeText(getActivity(), "Please enable storage permisstion", Toast.LENGTH_SHORT).show();
                    }
                }

            }
            break;

        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode==RESULT_OK ){
           if(requestCode==IMAGE_PICK_GALLERY_CODE){
               image_uri=data.getData();
               uploadProfileCoverPhoto(image_uri);
           }
           if(requestCode==IMAGE_PICK_CAMERA_CODE){
               uploadProfileCoverPhoto(image_uri);
           }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void uploadProfileCoverPhoto(Uri image_uri) {
        /*
        the parameter "image_uri' contains the uri of image picked either from camera or gallery
        we will use UID of the currently signed in user as name of the image so there will be only one image
        profile and one image for cover for each user

         */
        //path and name of iamge to be stored in firebase storage
        String filePathAndName=storagePath+""+profileOrCoverPhoto+"_"+user.getUid();
        StorageReference storageReference2nd=storageReference.child(filePathAndName);
        storageReference2nd.putFile(image_uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                //image is uploaded to storage , now get it's url anf store in user's database
                Task<Uri> uriTask=taskSnapshot.getStorage().getDownloadUrl();
                while (!uriTask.isSuccessful());
                Uri dowloadUri=uriTask.getResult();
                //check if image is uploaded or not and url is received
                if(uriTask.isSuccessful()){
                    //image uploaded
                    //add/update url in user's database
                    HashMap<String,Object> results=new HashMap<>();
                    /*
                    First Parameter is profileOrCoverPhoto that has value "image" which are keys in users database
                    where url of image will be in one of them
                    Second parameter contains the url of the image stored in firebase storage, this
                    url will be saved as value against key "image" or "cover"
                     */
                    results.put(profileOrCoverPhoto,dowloadUri.toString());
                    databaseReference.child(user.getUid()).updateChildren(results)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getActivity(), "Image Updated", Toast.LENGTH_SHORT).show();

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Error update Image", Toast.LENGTH_SHORT).show();

                        }
                    });
                }
                else{
                    //error
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Some Error occured", Toast.LENGTH_SHORT).show();
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void pickFromGallery() {
        //Pick from gallery
        Intent galleryIntent=new Intent(Intent.ACTION_PICK);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent,IMAGE_PICK_GALLERY_CODE);

    }

    private void pickFromCamera() {
        //Intent of picking image from device camera
        ContentValues values=new ContentValues();
        values.put(MediaStore.Images.Media.TITLE,"Temp Pic");
        values.put(MediaStore.Images.Media.DESCRIPTION,"Temp Description");
        //put image uri
        image_uri=getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,values);
        //intent to start camera
        Intent cameraIntent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,image_uri);
        startActivityForResult(cameraIntent,IMAGE_PICK_CAMERA_CODE);

    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }
    @SuppressLint("RestrictedApi")
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        if(menu instanceof MenuBuilder){
            MenuBuilder m = (MenuBuilder) menu;
            m.setOptionalIconsVisible(true);
        }
        super.onCreateOptionsMenu(menu,inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==R.id.mnu_logout){
            sendToLogin();
            checkUserStatus();
        }
        if(id==R.id.mnu_add_post){
            startActivity(new Intent(getActivity(), AddPostActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    private void sendToLogin() { //funtion
        GoogleSignInClient mGoogleSignInClient ;
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
        mGoogleSignInClient.signOut().addOnCompleteListener(getActivity(),
                new OnCompleteListener<Void>() {  //signout Google
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        FirebaseAuth.getInstance().signOut(); //signout firebase
                        Intent setupIntent = new Intent(getActivity(), LoginActivity.class/*To ur activity calss*/);
                        Toast.makeText(getActivity(), "Logged Out", Toast.LENGTH_LONG).show(); //if u want to show some text
                        setupIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(setupIntent);
                        getActivity().finish();
                    }
                });
    }

    private void checkUserStatus() {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if(user!=null){

        }
        else{
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finish();
        }

    }
}
