package ad.tranxuan.vinh.chatapp.Activities.Login;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

import ad.tranxuan.vinh.chatapp.Activities.DashboardActivity;
import ad.tranxuan.vinh.chatapp.Activities.Register.RegisterActivity;
import ad.tranxuan.vinh.chatapp.Presenter.Login.LoginImpl;
import ad.tranxuan.vinh.chatapp.Presenter.Login.LoginPresenter;
import ad.tranxuan.vinh.chatapp.R;
import ad.tranxuan.vinh.chatapp.View.Login.LoginView;

public class LoginActivity extends AppCompatActivity implements LoginView {
    private static final int RC_SIGN_IN =100 ;
    GoogleSignInClient mGoogleSignInClient;
    ImageView imgGoogle,imgFacebook,imgTwist;
    Button btnsignin,btnsignup,btnllogin;
    EditText txtemai,txtpassword;
    TextView txtForgotPass;
    private FirebaseAuth mAuth;
    //call presenter
    private ProgressDialog progressDialog;
    LoginPresenter loginPresenter;
    //String Global (Email,Passwork) for login(email,password)
    String email;
    String passWord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        progressDialog= new ProgressDialog(this);
        progressDialog.setTitle("Logging ...");
        loginPresenter=new LoginImpl(this);
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient=GoogleSignIn.getClient(this,gso);
        mAuth = FirebaseAuth.getInstance();
        ActionBar actionBar=getSupportActionBar();
        actionBar.hide();
        addControls();
        addEvents();
    }

    private void addEvents() {
        btnllogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email=txtemai.getText().toString();
                passWord=txtpassword.getText().toString().trim();
                loginPresenter.performLogin(email,passWord);
            }
        });

        btnsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });
        txtForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRecoverPassWordDialog();
            }
        });
        imgGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });


    }

    private void showRecoverPassWordDialog() {
        //Alertdialog
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Recover PassWord");
        //linearlayout
        LinearLayout linearLayout=new LinearLayout(this);
        //view to set in dialog
        final EditText txtEmailRecover=new EditText(this);
        txtEmailRecover.setHint("Enter Email");
        txtEmailRecover.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        txtEmailRecover.setMinEms(16);
        linearLayout.addView(txtEmailRecover);
        linearLayout.setPadding(10,10,10,10);
        builder.setView(linearLayout);
        //button recover
        builder.setPositiveButton("Recover", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String emailrecover=txtEmailRecover.getText().toString().trim();
                beginRecover(emailrecover);

            }
        });
        builder.setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();

    }

    private void beginRecover(String emailrecover) {
        progressDialog.setMessage("Sending Email...");
        progressDialog.show();
        mAuth.sendPasswordResetEmail(emailrecover).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                progressDialog.dismiss();
                if(task.isSuccessful()){
                    Toast.makeText(LoginActivity.this, "Email sent", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(LoginActivity.this, "Failed...", Toast.LENGTH_SHORT).show();
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.dismiss();
                //get and show proper error message
                Toast.makeText(LoginActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void login(String email,String pass){
        progressDialog.setMessage("Logging In...");
        progressDialog.show();
        mAuth.signInWithEmailAndPassword(email, pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            progressDialog.dismiss();
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
                            finish();

                        } else {
                            progressDialog.dismiss();
                            // If sign in fails, display a message to the user.

                            Toast.makeText(LoginActivity.this, "Authentication failed.",Toast.LENGTH_SHORT).show();

                        }

                        // ...
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.dismiss();
                Toast.makeText(LoginActivity.this,""+e.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void addControls() {
        btnsignin=findViewById(R.id.btnLsignin);
        btnsignup=findViewById(R.id.btnLsignup);
        btnllogin=findViewById(R.id.btnLLogin);
        txtemai=findViewById(R.id.txtLEmail);
        txtpassword=findViewById(R.id.txtLPassWord);
        txtForgotPass=findViewById(R.id.txtLForgotPassWord);
        imgGoogle=findViewById(R.id.imgLgoogle);
        imgFacebook=findViewById(R.id.imgLfacebook);
        imgTwist=findViewById(R.id.imgLtwitter);

    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
    @Override
    public void loginValidations() {
        Toast.makeText(this, "Please enter email and password !", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void loginSuccess() {
        login(email,passWord);

    }
    @Override
    public void validateEmail() {
        txtemai.setError("Invalid Email");
        txtemai.setFocusable(true);

    }
    @Override
    public void validatePassWord() {
        txtpassword.setError("Password length at least 6 characters");
        txtpassword.setFocusable(true);


    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Toast.makeText(this, "Google sign in failed", Toast.LENGTH_SHORT).show();

            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {


        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            if(task.getResult().getAdditionalUserInfo().isNewUser()){
                                String email=user.getEmail();
                                String uid=user.getUid();
                                HashMap<Object,String> hashMap=new HashMap<>();
                                hashMap.put("email",email);
                                hashMap.put("uid",uid);
                                hashMap.put("name","");
                                hashMap.put("onlineStatus","online");
                                hashMap.put("typingTo","noOne");
                                hashMap.put("phone","");
                                hashMap.put("image","");
                                hashMap.put("cover","");
                                FirebaseDatabase database=FirebaseDatabase.getInstance();
                                DatabaseReference reference=database.getReference("Users");
                                reference.child(uid).setValue(hashMap);
                            }

                            Toast.makeText(LoginActivity.this, ""+user.getEmail(), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
                            finish();
                           //pdateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.

                            Toast.makeText(LoginActivity.this, "Sign In Failed", Toast.LENGTH_SHORT).show();

                        }


                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(LoginActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
