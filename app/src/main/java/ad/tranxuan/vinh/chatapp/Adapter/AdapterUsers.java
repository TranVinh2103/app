package ad.tranxuan.vinh.chatapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import ad.tranxuan.vinh.chatapp.Activities.ChatActivity;
import ad.tranxuan.vinh.chatapp.Model.User;
import ad.tranxuan.vinh.chatapp.R;

public class AdapterUsers extends RecyclerView.Adapter<AdapterUsers.MyHolder> {
    Context context;
    List<User> userList;
    //contructor


    public AdapterUsers(Context context, List<User> userList) {
        this.context = context;
        this.userList = userList;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        //inflate layout (row_user.xml)
        View view= LayoutInflater.from(context).inflate(R.layout.row_users,viewGroup,false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder myHolder, final int i) {
        //getdata
        final String hisUid=userList.get(i).getUid();
        final String userImage=userList.get(i).getImage();
        String userName=userList.get(i).getName();
        final String userEmail=userList.get(i).getEmail();
        //set data
        myHolder.txtUsersName.setText(userName);
        myHolder.txtUsersEmail.setText(userEmail);
        try{
            Picasso.get().load(userImage)
                    .placeholder(R.drawable.ic_default_img)
                    .into(myHolder.imgUsersAvartar);

        }
        catch (Exception ex){

        }
        //handle item click
        myHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, ChatActivity.class);
                intent.putExtra("hisUid",hisUid);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    //view holder class
    class MyHolder extends RecyclerView.ViewHolder{
         ImageView imgUsersAvartar;
         TextView txtUsersName,txtUsersEmail;


        public MyHolder(@NonNull View itemView) {
            super(itemView);
            //init view
            imgUsersAvartar=itemView.findViewById(R.id.imgUsersAvartar);
            txtUsersName=itemView.findViewById(R.id.txtUsersName);
            txtUsersEmail=itemView.findViewById(R.id.txtUsersEmail);
        }
    }
}
