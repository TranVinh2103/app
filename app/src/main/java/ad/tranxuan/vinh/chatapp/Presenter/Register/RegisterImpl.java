package ad.tranxuan.vinh.chatapp.Presenter.Register;

import android.text.TextUtils;
import android.util.Patterns;

import ad.tranxuan.vinh.chatapp.View.Register.RegisterView;

public class RegisterImpl implements RegisterPresenter{
    RegisterView registerView;

    public RegisterImpl(RegisterView registerView) {
        this.registerView = registerView;
    }

    @Override
    public void performRegister(String email, String password,String confirmPass) {
        if(TextUtils.isEmpty(email)||TextUtils.isEmpty(password)){
            registerView.registerValidations();
        }
        else if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            registerView.validateEmail();
        }
        else if(password.length()<6){
            registerView.validatePassWord();
        }
        else if(!password.equals(confirmPass)){
            registerView.validateConfirmPass();
        }
        else{
            registerView.registerSuccess();
        }

    }

}
