package ad.tranxuan.vinh.chatapp.Notification;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers({
            "Content-Type:application/json",
            "Authorization:key=AAAAeQSoVFU:APA91bEeND9DLQ3h1mxGpQh0WTNV-WuRZs0RuNfSbTHwQBAg4zj0WsRju1928-ydPx5mQ8aZPQAm-CQmv9bdOUekNv_-0cfOtO-ZqVhMC_eCjEl3jQ4GUJ6bGYZ9NVCyTi9_fHd3QWoo"

    })
    @POST("fcm/send")
    Call<Response> sendNotification(@Body Sender body);
}
