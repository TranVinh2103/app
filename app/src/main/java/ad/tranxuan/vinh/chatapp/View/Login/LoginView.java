package ad.tranxuan.vinh.chatapp.View.Login;

public interface LoginView {
    void loginValidations();
    void loginSuccess();
    void validateEmail();
    void validatePassWord();

}
