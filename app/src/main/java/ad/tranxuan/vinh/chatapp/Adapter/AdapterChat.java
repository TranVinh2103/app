package ad.tranxuan.vinh.chatapp.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import ad.tranxuan.vinh.chatapp.Model.ModelChat;
import ad.tranxuan.vinh.chatapp.R;

public class AdapterChat extends RecyclerView.Adapter<AdapterChat.MyHolder> {
    private static final int MSG_TYPE_LEFT=0;
    private static final int MSG_TYPE_RIGHT=1;
    Context context;
    List<ModelChat> chatList;
    String imageUrl;
    FirebaseUser fUser;

    public AdapterChat(Context context, List<ModelChat> chatList, String imageUrl) {
        this.context = context;
        this.chatList = chatList;
        this.imageUrl = imageUrl;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if(i==MSG_TYPE_RIGHT){
            View view= LayoutInflater.from(context).inflate(R.layout.row_chat_right,viewGroup,false);
            return new MyHolder(view);
        }
        else{
            View view= LayoutInflater.from(context).inflate(R.layout.row_chat_left,viewGroup,false);
            return new MyHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder myHolder, final int i) {
        String message=chatList.get(i).getMessage();
        String timestamp=chatList.get(i).getTimestamp();

        Calendar calendar=Calendar.getInstance(Locale.ENGLISH);
        calendar.setTimeInMillis(Long.parseLong(timestamp));
        String dateTime= DateFormat.format("dd/MM/yyyy hh:mm aa",calendar).toString();
        //set data
        myHolder.txtShowMessage.setText(message);
        myHolder.txtTimeSend.setText(dateTime);
        try{
            Picasso.get().load(imageUrl).into(myHolder.imgRowChatProfile);
        }
        catch (Exception ex){

        }
        //click message to show delete dialog
        myHolder.messageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder=new AlertDialog.Builder(context);
                builder.setTitle("Delete");
                builder.setMessage("Are you sure to delete this message ?");
                //delete button
                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int position) {
                        deleteMessage(i);
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //dissmis dialog
                        dialogInterface.dismiss();
                    }
                });
                builder.create().show();
            }
        });

        //set seen status message
        if(i==chatList.size()-1){
            if(chatList.get(i).isSeen()){
                myHolder.txtIsSeen.setText("Seen");
            }
            else{
                myHolder.txtIsSeen.setText("Delivered");
            }
        }
        else {
            myHolder.txtIsSeen.setVisibility(View.GONE);
        }

    }

    /*
    deleteMessage(int position)
    1) get timestamp of clicked message
    2) compare the timestamp of the clicked message with all message in Chats
    3) Where both values matches delete that message
    4) This will allow sender to delete his and receiver's message

     */
    private void deleteMessage(int position) {
        final String myUid=FirebaseAuth.getInstance().getCurrentUser().getUid();
        String msgTimeStamp =chatList.get(position).getTimestamp();
        DatabaseReference dbRef= FirebaseDatabase.getInstance().getReference("Chats");
        Query query=dbRef.orderByChild("timestamp").equalTo(msgTimeStamp);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds:dataSnapshot.getChildren()){
                    if(ds.child("sender").getValue().equals(myUid)){
                         /*
                    we can do one of tow things here
                    1) remove the message from Chats
                    2) set the value of message "this message was deleted"
                     */
                        ds.getRef().removeValue();
//                        HashMap<String,Object> hashMap=new HashMap<>();
//                        hashMap.put("message","this message was deleted...");
//                        ds.getRef().updateChildren(hashMap);
//                        Toast.makeText(context, "Message was deleted...", Toast.LENGTH_SHORT).show();

                    }
                    else {
                        Toast.makeText(context, "You can delete only your message...", Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    @Override
    public int getItemViewType(int position) {
        fUser=FirebaseAuth.getInstance().getCurrentUser();
        if(chatList.get(position).getSender().equals(fUser.getUid())){
            return  MSG_TYPE_RIGHT;
        }
        else {
            return MSG_TYPE_LEFT;
        }
    }

    //view Holder
    class MyHolder extends RecyclerView.ViewHolder{
        //view
        ImageView imgRowChatProfile;
        TextView txtShowMessage,txtTimeSend,txtIsSeen;
        LinearLayout messageLayout;


        public MyHolder(@NonNull View itemView) {
            super(itemView);
            //init View
            imgRowChatProfile=itemView.findViewById(R.id.imgRowChatProfile);
            txtShowMessage=itemView.findViewById(R.id.txtShowMessage);
            txtIsSeen=itemView.findViewById(R.id.txtIsSeen);
            txtTimeSend=itemView.findViewById(R.id.txtTimeSend);
            messageLayout=itemView.findViewById(R.id.messageLayout);
        }


    }
}
