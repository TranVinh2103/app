package ad.tranxuan.vinh.chatapp.Presenter.Register;

public interface RegisterPresenter {
    void performRegister(String email,String password,String confirmPass);
}
