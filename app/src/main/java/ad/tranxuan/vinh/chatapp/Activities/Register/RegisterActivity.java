package ad.tranxuan.vinh.chatapp.Activities.Register;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

import ad.tranxuan.vinh.chatapp.Activities.Login.LoginActivity;
import ad.tranxuan.vinh.chatapp.Presenter.Register.RegisterImpl;
import ad.tranxuan.vinh.chatapp.Presenter.Register.RegisterPresenter;
import ad.tranxuan.vinh.chatapp.R;
import ad.tranxuan.vinh.chatapp.View.Register.RegisterView;

public class RegisterActivity extends AppCompatActivity implements RegisterView {
    Button btnRSignIn,btnRSignUp,btnRegister;
    EditText txtREmail,txtRPassWord,txtRConfirmPass;
    TextView txtHaveAccount;
    ImageView imgcheckpass,imgcheckconfirm;
   // Declare an instance of FirebaseAuth
    private FirebaseAuth mAuth;
    ProgressDialog progressDialog;
    private RegisterPresenter registerPresenter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ActionBar actionBar=getSupportActionBar();
        actionBar.hide();
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Registering user ...");
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        addControls();
        addEvents();
        registerPresenter=new RegisterImpl(this);
        imgcheckpass.setVisibility(View.INVISIBLE);
        imgcheckconfirm.setVisibility(View.INVISIBLE);
    }
    private void addControls() {
        imgcheckpass=findViewById(R.id.imgcheckpass);
        imgcheckconfirm=findViewById(R.id.imgcheckconfirm);
        btnRSignIn=findViewById(R.id.btnRSignIn);
        btnRegister=findViewById(R.id.btnRRegister);
        btnRSignUp=findViewById(R.id.btnRSignUp);
        txtREmail=findViewById(R.id.txtREmail);
        txtRPassWord=findViewById(R.id.txtRPassWord);
        txtRConfirmPass=findViewById(R.id.txtRCofirmPass);
        txtHaveAccount=findViewById(R.id.txtHaveAccount);
    }

    private void addEvents() {
        btnRSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email=txtREmail.getText().toString();
                String passWord=txtRPassWord.getText().toString();
                String confirmPass=txtRConfirmPass.getText().toString();
                registerPresenter.performRegister(email,passWord,confirmPass);

            }
        });
        txtHaveAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));

            }
        });


    }

    private boolean sendEmail(){
        FirebaseUser user=mAuth.getCurrentUser();
        user.sendEmailVerification().addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
            }
        });
        return true;
    }

    private void registerUser(final String email, String passWord) {
        progressDialog.show();
        mAuth.createUserWithEmailAndPassword(email,passWord)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    //sendEmail();
                    progressDialog.dismiss();
                    FirebaseUser user=mAuth.getCurrentUser();
                    String email=user.getEmail();
                    String uid=user.getUid();
                    HashMap<Object,String> hashMap=new HashMap<>();
                    hashMap.put("email",email);
                    hashMap.put("uid",uid);
                    hashMap.put("name","");
                    hashMap.put("onlineStatus","online");
                    hashMap.put("typingTo","noOne");
                    hashMap.put("phone","");
                    hashMap.put("image","");
                    hashMap.put("cover","");
                    FirebaseDatabase database=FirebaseDatabase.getInstance();
                    DatabaseReference reference=database.getReference("Users");
                    reference.child(uid).setValue(hashMap);
                    startActivity(new Intent(RegisterActivity.this, LoginActivity.class));

                }
                else {
                    Toast.makeText(RegisterActivity.this, "Authentication failed ", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }


    @Override
    public void registerValidations() {
        Toast.makeText(this, "Please enter email and password ", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void registerSuccess() {
        registerUser(txtREmail.getText().toString(),txtRPassWord.getText().toString());

    }

    @Override
    public void validateEmail() {
        txtREmail.setError("Invalid Email");
        txtREmail.setFocusable(true);

    }

    @Override
    public void validatePassWord() {
        txtRPassWord.setError("Password length at least 6 characters");
        txtRPassWord.setFocusable(true);

    }

    @Override
    public void validateConfirmPass() {
        txtRConfirmPass.setError("Confirm password failed ");
        txtRConfirmPass.setFocusable(true);

    }


}
