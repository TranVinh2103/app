package ad.tranxuan.vinh.chatapp.Presenter.Login;

public interface LoginPresenter {
    void performLogin(String email,String password);
}
