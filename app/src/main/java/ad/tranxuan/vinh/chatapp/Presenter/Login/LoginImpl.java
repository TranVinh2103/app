package ad.tranxuan.vinh.chatapp.Presenter.Login;

import android.app.ProgressDialog;
import android.text.TextUtils;
import android.util.Patterns;

import ad.tranxuan.vinh.chatapp.Presenter.Login.LoginPresenter;
import ad.tranxuan.vinh.chatapp.View.Login.LoginView;

public class LoginImpl implements LoginPresenter {
    LoginView lgView;

    public LoginImpl(LoginView lgView) {
        this.lgView = lgView;
    }

    @Override
    public void performLogin(String email, String password) {
        if(TextUtils.isEmpty(email)||TextUtils.isEmpty(password)){
            lgView.loginValidations();
        }
        else if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            lgView.validateEmail();
        }
        else if(password.length()<6){
            lgView.validatePassWord();
        }
        else{
            lgView.loginSuccess();
        }

    }
}
